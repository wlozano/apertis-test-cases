metadata:
  name: cgroups-resource-control
  format: "Apertis Test Definition 1.0"
  image-types:
    target:  [ armhf-internal, armhf, amd64 ]
    basesdk: [ amd64 ]
    sdk:     [ amd64 ]
  image-deployment:
    - APT
    - OSTree
  type: functional
  exec-type: automated
  priority: medium
  maintainer: "Apertis Project"
  description: "Run cgroup resource control tests"

  pre-conditions:
    - "If running inside VirtualBox, disable the host I/O cache under \"storage\"
       in the image settings. Not doing so can cause the test_blkio_* tests to
       fail."

  expected:
    - "The output should be similar to that:"
    - |
        >>>> Test setup successfully!
        >>> Running test 'test_cpu_shares' ... PASS
        >>> Running test 'test_memory_limits' ... PASS
        >>> Running test 'test_memory_threshold_notification' ... PASS
        >>> Running test 'test_blkio_weights_random_read' ... PASS
        >>> Running test 'test_blkio_weights_sequential_read' ... PASS
        >>> Running test 'test_network_cgroup_priority_classification' ... PASS
        >>> All tests PASSED successfully!
    - "If any test failed, they will be listed instead of the success message:"
    - |
        >>>> The following tests FAILED:
        [list of tests]

install:
  git-repos:
    - url: https://gitlab.apertis.org/tests/cgroups-resource-control.git
      branch: 'apertis/v2022dev0'

run:
  steps:
    - "# Enter test directory:"
    - cd cgroups-resource-control
    - "# Execute the following commands:"
    - common/run-test-in-systemd --name=cpu-shares --timeout=900 env DEBUG=2 ./cgroups-resource-control.sh test_cpu_shares
    - common/run-test-in-systemd --name=memory-limits --timeout=900 env DEBUG=2 ./cgroups-resource-control.sh test_memory_limits
    - common/run-test-in-systemd --name=memory-threshold-notification --timeout=900 env DEBUG=2 ./cgroups-resource-control.sh test_memory_threshold_notification
    - common/run-test-in-systemd --name=blkio-weights-random-read --timeout=900 env DEBUG=2 ./cgroups-resource-control.sh test_blkio_weights_random_read
    - common/run-test-in-systemd --name=blkio-weights-seq-read --timeout=900 env DEBUG=2 ./cgroups-resource-control.sh test_blkio_weights_sequential_read
    - common/run-test-in-systemd --name=network-cgroup-prio-class --timeout=900 env DEBUG=2 ./cgroups-resource-control.sh test_network_cgroup_priority_classification

parse:
  pattern: ^(?P<test_case_id>[a-zA-Z0-9_\-\./]+):\s*(?P<result>pass|fail|skip|unknown)$
