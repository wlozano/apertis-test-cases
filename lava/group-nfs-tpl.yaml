job_name: Nfsroot tests on {{release_version}} {{pretty}} {{image_date}}

device_type: {{device_type}}
priority: medium
{% macro kernel_type(arch) -%}
    {% if arch == 'arm64' -%}
        image
    {%- else -%}
        zimage
    {%- endif %}
{%- endmacro %}

visibility: {{visibility}}

notify:
  callback:
    url: https://lavaphabbridge.apertis.org/
    method: POST
    dataset: results
    content-type: json
    token: lava-phab-bridge
  criteria:
    status: finished

timeouts:
  job:
    minutes: 180
  action:
    minutes: 120
  connections:
    overlay-unpack:
      minutes: 20

context:
  kernel_start_message: '.*'

metadata:
  source.project: '{{source_project}}'
  source.ref: '{{source_ref}}'
  source.commit: '{{source_commit}}'
  source.pipeline: '{{source_pipeline}}'
  source.job: '{{source_job}}'
  source.wip: {{source_wip}}
  image.version: '{{image_date}}'
  image.release: '{{release_version}}'
  image.arch: '{{arch}}'
  image.board: '{{board}}'
  image.type: '{{image_type}}'

actions:
  - deploy:
      namespace: nfsboot
      timeout:
        minutes: 15
      to: tftp
      kernel:
        url: {{baseurl}}/{{imgpath}}/{{image_date}}/{{arch}}/nfs/vmlinuz
        type: {{kernel_type(arch)}}
      nfsrootfs:
        url: {{baseurl}}/{{imgpath}}/{{image_date}}/{{arch}}/nfs/apertis-nfs-{{arch}}.tar.gz
        compression: gz
      ramdisk:
        url: {{baseurl}}/{{imgpath}}/{{image_date}}/{{arch}}/nfs/initrd.img
        compression: gz
      os: ubuntu
      {% if needs_dtb -%}
      dtb:
        url: {{baseurl}}/{{imgpath}}/{{image_date}}/{{arch}}/nfs/dtbs/{{dtb}}
      {%- endif %}

  - boot:
      namespace: nfsboot
      timeout:
        minutes: 30 # Minnowboards can spend a long time clearing memory due to MRC requests
      method: {{boot_method}}
      commands: nfs
      transfer_overlay:
        download_command: export SHELL=/bin/sh; sudo mount -o remount,rw / ; cd /tmp ; busybox wget
        unpack_command: sudo tar -C / -xf
      auto_login:
        login_prompt: 'apertis login:'
        username: user
        password_prompt: 'Password:'
        password: user
        login_commands:
          - sudo su
      prompts:
        - '\$ '
        - '/home/user #'
        - '/tmp #'

  - test:
      timeout:
        minutes: 3
      namespace: nfsboot
      name: nfsroot-simple-boot
      definitions:
        - repository: https://gitlab.apertis.org/tests/apertis-test-cases.git
          revision: 'apertis/v2022dev0'
          from: git
          path: test-cases/nfsroot-simple-boot.yaml
          name: nfsroot-simple-boot
